#pragma once
#include "Stop.h"

/*!
* \class Stop
* \brief Valve class is derived from Stop. 
* In this way, the object base logic for the valve is established. 
* In this way, we can make a pattern design.
*/

typedef class Valve : public Stop{
private:
	bool valve;
public:
	void OpenValve(){
		this->valve = true;
	}
	void CloseValve() {
		this->valve = false;
	}
	bool IsOpenedValve() const {
		return this->valve;
	}
}Valve;