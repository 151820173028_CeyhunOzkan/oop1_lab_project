#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <string>
#include "Engine.h"
#include <Windows.h>
#include <time.h>
#include "OutputWriter.h"
using namespace std;
using namespace Simulation;

/*!
* \brief Constructor method.
*/
Engine::Engine(double capacity) : Tank(capacity)
{
	this->status = false;
}
/*!
* \brief Destructor method.
*/
Engine::~Engine() {

}

Engine* Engine::instance = NULL;
/*!
* \brief Singleton pattern.
*/
Engine* Engine::GetInstance()
{
	if (!Engine::instance)
		Engine::instance = new Engine(55.0);

	return Engine::instance;
}

/*!
* \brief Method starts engine.
* @return nothing.
*/
void Engine::Start() 
{
	//1. Ba�l� ve kapa�� a��k tank olacak.
	//2. Bu tankta yak�t olacak.
	for (int i = 0; i < this->tanks.Length(); i++)
	{
		if (this->tanks.GetItem(i).GetConnectedStatus() == "Engine Connected"
			&& this->tanks.GetItem(i).IsOpenedValve() == true)
		{
			if (this->GetFuelQuantity() >= this->fuelPerSecond)
			{
				this->status = true;
			}
		}

	}

	if (this->status)
		OutputWriter::WriteLine(Main::outputFileName, "->> The engine started running.\n");
	else
		OutputWriter::WriteLine(Main::outputFileName, "->> Engine was attempted to start but could not be started.\n");
}
/*!
* \brief Method stops engine.
* @return nothing.
*/
void Engine::Stop() {
	this->status = false;
	OutputWriter::WriteLine(Main::outputFileName, "---> The engine was stopped!\n");
	this->GiveBackFuel(this->GetFuelQuantity());
}
/*!
* \brief Method adds tank to engine.
* @param capacity is double variable.
* @return nothing.
*/
void Engine::AddTank(double capacity)
{
	Tank t = Tank(capacity);

	this->tanks.Add(t);
	OutputWriter::WriteLine(Main::outputFileName, "* Tank that tank ID is " + t.GetTankID() + " was added.\n");
}
/*!
* \brief Method removes tanks from engine.
* @param tankID is string variable.
* @return nothing.
*/
void Engine::RemoveTank(string tankID) {
	int index = this->GetTank(tankID);
	this->tanks.Remove(index);
	//e�er index yoksa hata vermeli -1;
	if (index != -1)
		OutputWriter::WriteLine(Main::outputFileName, "*! Tank that tank ID is " + tankID + " was removed!\n");
}
/*!
* \brief Method lists all tanks.
* @return nothing.
*/
void Engine::ListTanks() {

	for (int i = 0; i < this->tanks.Length(); i++)
	{
		cout << this->tanks.GetItem(i) << endl;
	}
}

/*!
* \brief Method connects engine and tank.
* @param tankID is string variable.
* @return nothing.
*/
void Engine::Connect(string tankID) {
	int index = this->GetTank(tankID);
	if (index == -1) {} //b�yle bir tank numaras�na ait tank yok
	else {
		Tank t = this->tanks.GetItem(index);
		t.SetConnectedStatus("Engine Connected");
		this->tanks.Remove(index);
		this->tanks.Insert(index, t);
		this->TransferFuel();
		OutputWriter::WriteLine(Main::outputFileName, "** Tank that tank ID is " + t.GetTankID() + " was connected to engine.\n");
	}
}
/*!
* \brief Method disconnects engine and tank.
* @param tankID is string variable.
* @return nothing.
*/
void Engine::Disconnect(string tankID) {
	int index = this->GetTank(tankID);
	Tank t = this->tanks.GetItem(index);
	t.SetConnectedStatus("No connected");
	this->tanks.Remove(index);
	this->tanks.Insert(index, t);
	
	if (index != -1)
		OutputWriter::WriteLine(Main::outputFileName, "*! Tank that tank ID is " + t.GetTankID() + " was disconnected to engine.\n");
	//e�er index -1 ise ba�lant�s�n� kesemezsin!
}
/*!
* \brief Method prints the number of tanks.
* @return nothing.
*/
void Engine::PrintTankCount() const
{
	string data = "---> Tank Count: " + to_string(this->tanks.Length()) + "\n";
	cout << data; //Print to screen.
	OutputWriter::WriteLine(Main::outputFileName, data); //Print to file.
}
/*!
* \brief Method gets the tank.
* @param tankID is string variable.
* @return index if there is a tank with that tankID.
*/
int Engine::GetTank(string tankID) const {
	for (int i = 0; i < this->tanks.Length(); i++)
	{
		if (this->tanks.GetItem(i).GetTankID() == tankID)
			return i;
	}

	return -1;
}
/*!
* \brief Method fills the tank.
* @param quantity is double variable and @param tankID is string variable.
* @return nothing.
*/
void Engine::FillTank(double quantity, string tankID)
{
	int index = this->GetTank(tankID);
	Tank t = this->tanks.GetItem(index);
	this->tanks.Remove(index);
	if ((t.GetFuelQuantity() + quantity) <= t.GetTankCapacity())
	{
		t.SetFuelQuantity(t.GetFuelQuantity() + quantity);

		OutputWriter::WriteLine(Main::outputFileName, "*+ " + to_string(quantity) + " liter fuel was filled to the tank that tank ID is " + t.GetTankID() + ".\n");
	}
	else {
		OutputWriter::WriteLine(Main::outputFileName, "*! Filling operation failed. Tank ID of target tank is " + tankID + ".\n");
	}// !!! warning;
	this->tanks.Insert(index, t);
}
/*!
* \brief Method transfers fuel to the engine when it falls below 20 quantity.
* @return nothing.
*/
void Engine::TransferFuel()
{
	if (this->GetFuelQuantity() < 20)
	{
		int random;
		List<int> indices = this->GetTank();
		if (indices.Length() == 0)
			return;
		random = rand() % indices.Length();

		Tank t = this->tanks.GetItem(indices.GetItem(random));

		if (t.GetFuelQuantity() != 0)
		{
			if (this->GetTankCapacity() - this->GetFuelQuantity() <= t.GetFuelQuantity()) // gereken benzin <=tank�n benzininden
			{
				double amount = t.GetFuelQuantity() - (this->GetTankCapacity() - this->GetFuelQuantity());
				this->SetFuelQuantity(this->GetFuelQuantity() + t.GetFuelQuantity() - amount);
				t.SetFuelQuantity(amount);
			}
			else
			{
				this->SetFuelQuantity(this->GetFuelQuantity() + t.GetFuelQuantity());
				t.SetFuelQuantity(0);

			}
			/*
			1.durum tankta gerekenden fazla benzin var.150cp , motor 55cp , gerekli benzin 55litre;
			2.durum tankta gerekenden az benzin var. 30cp, motor 55cp, gerekli benzin 30litre; Tank=0

			*/
		}
		this->tanks.Remove(random);
		this->tanks.Insert(random, t);
	}
}
/*!
* \brief Method calculates the fuel consumed in seconds with the engine running.
* @return nothing.
*/
void Engine::Running()
{
	Sleep(1000);
	if (this->GetFuelQuantity() >= this->fuelPerSecond) {
		this->SetFuelQuantity(this->GetFuelQuantity() - this->fuelPerSecond);
		this->consumedFuel += this->fuelPerSecond;
	}

	this->TransferFuel();
	
	if (this->GetFuelQuantity() < this->fuelPerSecond)
		this->Stop();

}
/*!
* \brief Method gets the number of tanks connected to the engine.
* @return the number of connected tanks to the engine.
*/
int Engine::GetConnectedTanksCount()
{
	int ct = 0;
	for (int i = 0; i < this->tanks.Length(); i++)
	{
		if (this->tanks.GetItem(i).GetConnectedStatus() == "Engine Connected")
			ct++;
	}
	return ct;
}
/*!
* \brief Method opens the valve of tank.
* @param tankID is string variable.
* @return nothing.
*/
void Engine::OpenTankValve(string tankID)
{
	int index = this->GetTank(tankID);
	Tank t = this->tanks.GetItem(index);
	t.OpenValve();
	this->tanks.Remove(index);
	this->tanks.Insert(index, t);

	OutputWriter::WriteLine(Main::outputFileName, "*# The valve of tank that tank ID is " + t.GetTankID() + " was opened.\n");

	this->TransferFuel();
}
/*!
* \brief Method if the tank is connected to the engine and its valve is open, it adds it to the list.
* @return list.
*/
List<int> Engine::GetTank()const
{
	List<int> list;
	for (int i = 0; i < this->tanks.Length(); i++)
	{
		Tank t = this->tanks.GetItem(i);
		if (t.GetConnectedStatus() == "Engine Connected" && t.IsOpenedValve() == 1)
			list.Add(i);
	}
	return list;
}
/*!
* \brief Method prints the tank informations.
* @param tankID is string variable.
* @return nothing.
*/
void Engine::PrintTankInfo(string tankID) const {
	int index = this->GetTank(tankID);
	cout << this->tanks.GetItem(index) << endl;
}
/*!
* \brief Method prints total fuel quantity.
* @return nothing.
*/
void Engine::PrintTotalFuelQuantity() const {
	double total = 0;
	for (int i = 0; i < this->tanks.Length(); i++) {

		total += this->tanks.GetItem(i).GetFuelQuantity();
	}
	total += this->GetFuelQuantity();
	cout << "Total Fuel Quantity: " << total << endl;
	OutputWriter::WriteLine(Main::outputFileName, "---> Total Fuel Quantity: " + to_string(total) + "\n");
}
/*!
* \brief Method prints total consumed fuel quantity.
* @return nothing.
*/
void Engine::PrintTotalConsumedFuelQuantity() const {
	cout << "Total Consumed Fuel Quantity: " << this->consumedFuel << endl;
	OutputWriter::WriteLine(Main::outputFileName, "---> Total Consumed Fuel Quantity: " + to_string(this->consumedFuel) + "\n");
}
/*!
* \brief Method waits the engine for the seconds entered.
* @return nothing.
*/
void Engine::Wait(int sec)
{
	OutputWriter::WriteLine(Main::outputFileName, "---> Engine is being waited during " + to_string(sec) + " second\n");
	cout << "--->Engine is being waited during " + to_string(sec) + " second\n";
	Sleep(sec * 1000);
}
/*!
* \brief Method checks whether the engine is connected to the tank.
* @return true or false.
*/
bool Engine::GetEngineStatus() const
{
	return this->status;
}

/*!
* \brief Method gives back fuel before the engine stops.
* @param quantity is double variable.
* @return nothing.
*/
void Engine::GiveBackFuel(double quantity)
{
	double minimize = -1;
	int index;
	for (int i = 0; i < this->tanks.Length(); i++)
	{
		if (this->tanks.GetItem(i).GetConnectedStatus() == "Engine Connected"
			&& this->tanks.GetItem(i).IsOpenedValve() == true)
		{
			if (minimize == -1)
			{
				minimize = this->tanks.GetItem(i).GetFuelQuantity();
				index = i;
			}

			if (minimize > this->tanks.GetItem(i).GetFuelQuantity())
			{
				minimize = this->tanks.GetItem(i).GetFuelQuantity();
				index = i;
			}
		}
	}

	Tank t = this->tanks.GetItem(index);
	t.SetFuelQuantity(t.GetFuelQuantity() + quantity);
	this->tanks.Remove(index);
	this->tanks.Insert(index, t);
	OutputWriter::WriteLine(Main::outputFileName, "*+ " + to_string(quantity) + 
		"liter fuel was added back to the tank that tank ID is " + t.GetTankID() + ".\n");
}

/*!
* \brief After the simulation was stopped, firstly, engine shutdown was printed on the console and output screen. 
* Then, stopping the tanks and stopping the valves of the tanks were written.
* @return nothing.
*/
void Engine::StopSimulation() {
	string info = "Engine: Simulation stopped\n";
	cout << info;
	OutputWriter::WriteLine(Main::outputFileName, info);
	for (int i = 0; i < this->tanks.Length(); i++) {
		this->tanks.GetItem(i).StopSimulation();
	}
}