#pragma once
#include <stdlib.h>
#include <iostream>
#include <string>
#include "List.h"
#include "InputReader.h"
#include "Engine.h"
#include "Tank.h"
#include "Utility.h"
#include <Windows.h>
#include <time.h>

using namespace std;
using namespace Simulation;
/*!
* \class InputReader
* \brief Includes Main constructor method and Compile method.
*/
typedef class Main
{
public:
	static string ErrorMessage;
	static string outputFileName;
	Engine* engine;

	Main();
	void Compile(string file);
}Main;
