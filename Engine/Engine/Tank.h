#pragma once
#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <string>
#include "Stop.h"
#include "Valve.h"
using namespace std;

/*!
* \class Tank
* \brief Includes methods of Repair,Break,OpenValve,CloseValve,IsBroken,IsOpenedValve,SetTankCapacity,GetTankCapacity,SetFuelQuantity,
*  GetFuelQuantity,GetTankID,SetConnectedStatus,GetConnectedStatus.
*/
typedef class Tank : public Valve{
private:
	friend ostream& operator<<(ostream&, const Tank&);
	bool broken;
	double capacity;
	double fuelQuantity;
	string tankID;
	string connected;
	static int ID;
public:
	Tank();
	Tank(double capacity);
	~Tank();
	void RepairTank();
	void BreakTank();	
	bool IsBroken();
	void SetTankCapacity(double);
	double GetTankCapacity() const;
	void SetFuelQuantity(double);
	double GetFuelQuantity() const;
	string GetTankID() const;
	void SetConnectedStatus(string);
	string GetConnectedStatus() const;
	virtual void StopSimulation();


}Tank;
