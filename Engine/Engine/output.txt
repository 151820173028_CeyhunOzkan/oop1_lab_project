->> Engine was attempted to start but could not be started.
* Tank that tank ID is 1 was added.
* Tank that tank ID is 2 was added.
* Tank that tank ID is 3 was added.
* Tank that tank ID is 4 was added.
* Tank that tank ID is 5 was added.
*+ 100.000000 liter fuel was filled to the tank that tank ID is 1.
*+ 150.000000 liter fuel was filled to the tank that tank ID is 2.
->> Engine was attempted to start but could not be started.
*+ 100.000000 liter fuel was filled to the tank that tank ID is 3.
** Tank that tank ID is 1 was connected to engine.
** Tank that tank ID is 2 was connected to engine.
** Tank that tank ID is 3 was connected to engine.
** Tank that tank ID is 4 was connected to engine.
*! Tank that tank ID is 5 was removed!
*! Tank that tank ID is 4 was disconnected to engine.
->> Engine was attempted to start but could not be started.
*# The valve of tank that tank ID is 1 was opened.
*# The valve of tank that tank ID is 2 was opened.
*! Filling operation failed. Tank ID of target tank is 1.
*! Filling operation failed. Tank ID of target tank is 2.
*! Filling operation failed. Tank ID of target tank is 3.
->> The engine started running.
---> Engine is being waited during 5 second

---> Tank ID: 1<---
Capacity: 100.000000
Fuel Quantity: 45.000000
Status: Solid
Valve: Open
Connected: Engine Connected

---> Tank ID: 2<---
Capacity: 150.000000
Fuel Quantity: 150.000000
Status: Solid
Valve: Open
Connected: Engine Connected

---> Tank ID: 3<---
Capacity: 100.000000
Fuel Quantity: 100.000000
Status: Solid
Valve: Open
Connected: Engine Connected

---> Tank ID: 4<---
Capacity: 250.000000
Fuel Quantity: 0.000000
Status: Solid
Valve: Open
Connected: No connected
---> Tank Count: 4
---> Total Fuel Quantity: 322.500000
---> Total Consumed Fuel Quantity: 33.000000

---> Tank ID: 1<---
Capacity: 100.000000
Fuel Quantity: 45.000000
Status: Solid
Valve: Open
Connected: Engine Connected

---> Tank ID: 2<---
Capacity: 150.000000
Fuel Quantity: 111.500000
Status: Solid
Valve: Open
Connected: Engine Connected
---> Engine is being waited during 5 second
*! Filling operation failed. Tank ID of target tank is 1.
*! Filling operation failed. Tank ID of target tank is 2.
*! Filling operation failed. Tank ID of target tank is 3.

---> Tank ID: 1<---
Capacity: 100.000000
Fuel Quantity: 6.500000
Status: Solid
Valve: Open
Connected: Engine Connected

---> Tank ID: 2<---
Capacity: 150.000000
Fuel Quantity: 111.500000
Status: Solid
Valve: Open
Connected: Engine Connected
->> The engine started running.

---> Tank ID: 3<---
Capacity: 100.000000
Fuel Quantity: 100.000000
Status: Solid
Valve: Open
Connected: Engine Connected
---> The engine was stopped!
*+ 33.000000liter fuel was added back to the tank that tank ID is 1.

---> Tank ID: 1<---
Capacity: 100.000000
Fuel Quantity: 39.500000
Status: Solid
Valve: Open
Connected: Engine Connected

---> Tank ID: 2<---
Capacity: 150.000000
Fuel Quantity: 111.500000
Status: Solid
Valve: Open
Connected: Engine Connected
Engine: Simulation stopped
Tank 1: Simulation stopped
Valve 1: Simulation stopped
Tank 2: Simulation stopped
Valve 2: Simulation stopped
Tank 3: Simulation stopped
Valve 3: Simulation stopped
Tank 4: Simulation stopped
Valve 4: Simulation stopped
