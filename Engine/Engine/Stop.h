#pragma once
/*!
* \class Stop
* \brief Stop class created and Observer pattern implemented
*/
typedef class Stop {
public:
	virtual void StopSimulation() = 0;
}Stop;