#pragma once
#include <iostream>
#include <stdlib.h>
#include "List.h"
#include <string>
#include "Tank.h"
using namespace std;

/*!
* \namespace Simulation
* \class Engine
* \brief Includes methods of Start,Stop,Running,AddTank,RemoveTank,ListTanks,AbsorbFuel,GiveBackFuel,Connect,Disconnect,PrintTankCount,FillTank,
*  GetTank,GetTankGetConnectedTanksCount,OpenTankValve,PrintTankInfo,PrintTotalFuelQuantity,PrintTotalConsumedFuelQuantity.
*/
namespace Simulation
{
	typedef class Engine : public Tank {
	private:
		bool status; //engine
		const double fuelPerSecond = 5.5;
		List<Tank> tanks;
		static Engine* instance;
		Engine(double capacity);
		double consumedFuel = 0;
		void TransferFuel();
	public:
		~Engine();
		static Engine* GetInstance();
		void Start();
		void Stop();
		void Running();
		void AddTank(double);
		void RemoveTank(string tankID);
		void ListTanks();
		void AbsorbFuel(double);
		void GiveBackFuel(double quantity);
		void Connect(string tankID);
		void Disconnect(string tankID);
		void PrintTankCount() const;
		void FillTank(double quantity, string tankID);
		List<int> GetTank()const;
		int GetTank(string tankID) const;
		int GetConnectedTanksCount();
		void OpenTankValve(string tankID);
		void PrintTankInfo(string tankID) const;
		void PrintTotalFuelQuantity() const;
		void PrintTotalConsumedFuelQuantity() const;
		void Wait(int sec);
		bool GetEngineStatus() const;
		void StopSimulation();

	}Engine;
}
