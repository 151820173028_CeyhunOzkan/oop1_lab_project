#include <stdlib.h>
#include <iostream>
#include <string>
#include "List.h"
#include "InputReader.h"
#include "Engine.h"
#include "Tank.h"
#include "Utility.h"
#include <Windows.h>
#include <time.h>
#include "Main.h"
#include "OutputWriter.h"

using namespace std;
using namespace Simulation;

string Main::ErrorMessage;
string Main::outputFileName;
/*!
* \brief constructor method.
*/
Main::Main()
{
	string fileName;
	cout << "Enter the name of the command file: ";
	getline(cin, fileName);
	cout << "Enter the name of the output file: ";
	getline(cin, Main::outputFileName);

	remove(Main::outputFileName.c_str());

	this->engine = Engine::GetInstance();
	srand(time(NULL));

	


	Compile(fileName);
	cout << endl;
	system("pause");
}
/*!
* \brief Method compiles user-entered commands.
* @param file is string variable.
* @return nothing.
*/
void Main::Compile(string file)
{
	InputReader* reader = new InputReader(file); //Create an InputReader class to read the file.

	List<string> commands = reader->GetCommands(); //Pull the all commands in the file.

	for (int i = 0; i < commands.Length(); i++) //Loop until the number of commands.
	{
		string cmd = commands.GetItem(i); //Gets each command.

		if (this->engine->GetEngineStatus())
			this->engine->Running();

		List<string> dts = Utility::Split(cmd); //Split each command.
		string rawcode = dts.GetItem(0); //Get the notation of the command.
		Codes code = Utility::Transform(rawcode); //Transform the notation into the enum code.

		switch (code) //Loop switch
		{
		case Start_Engine: //If code equals to "start_engine;"
			this->engine->Start(); // Engine.Start();
			break;
		case Stop_Engine:
			this->engine->Stop();
			break;
		case Add_Fuel_Tank:
			this->engine->AddTank(Utility::PureNumber(dts.GetItem(1)));
			break;
		case List_Fuel_Tanks:
			this->engine->ListTanks();
			break;
			//...continue
		case Print_Fuel_Tank_Count:
			this->engine->PrintTankCount();
			break;
		case Remove_Fuel_Tank:
			this->engine->RemoveTank(to_string((int)Utility::PureNumber(dts.GetItem(1))));
			break;
		case Connect_Fuel_Tank_to_Engine:
			this->engine->Connect(to_string((int)Utility::PureNumber(dts.GetItem(1))));
			break;
		case Disconnect_Fuel_Tank_from_Engine:
			this->engine->Disconnect(to_string((int)Utility::PureNumber(dts.GetItem(1))));
			break;
		case List_Connected_Tanks:
			break;
		case Give_Back_Fuel:
			break;
		case Print_Total_Fuel_Quantity:
			this->engine->PrintTotalFuelQuantity();
			break;
		case Print_Total_Consumed_Total_Quantity:
			this->engine->PrintTotalConsumedFuelQuantity();
			break;
		case Print_Tank_Info:
			this->engine->PrintTankInfo((to_string((int)Utility::PureNumber(dts.GetItem(1)))));
			break;
		case Fill_Tank:
			this->engine->FillTank(Utility::PureNumber(dts.GetItem(2)), to_string((int)Utility::PureNumber(dts.GetItem(1))));
			break;
		case Open_Valve:
			this->engine->OpenTankValve(to_string((int)Utility::PureNumber(dts.GetItem(1))));
			break;
		case Close_Valve:
			//this->engine->CloseTankValve(to_string((int)Utility::PureNumber(dts.GetItem(1))));
			break;
		case Break_Fuel_Tank:
			break;
		case Repair_Fuel_Tank:
			break;
		case Wait:
			this->engine->Wait(round((int)Utility::PureNumber(dts.GetItem(1))));
			break;
		case Stop_Simulation:
			this->engine->StopSimulation();
			return;
		case NO_COMMAND:
			break;
		}
	}
}

void main()
{

	Main m;
}
