#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <string>
#include "List.h"
#include "InputReader.h"
#include "Main.h"
#include "OutputWriter.h"

using namespace std;
/*!
* \brief Method writes lines.
* @param fileName is string variable and @param line is string variable.
* @return nothing.
*/
void OutputWriter::WriteLine(string fileName, string line)
{
	ofstream stream = ofstream(fileName, ios::app);

	if (stream.is_open())
		stream << line;
	else
		Main::ErrorMessage = "The data cannot be written into the file.";

	stream.close();
}
/*!
* \brief Method deletes filename in folder.
* @param fileName is string variable.
* @return nothing.
*/
void OutputWriter::Delete(string fileName)
{
	int i = remove(fileName.c_str());
	if (i != 0)
		Main::ErrorMessage = "There is already exist, but cannot be deleted!";
}