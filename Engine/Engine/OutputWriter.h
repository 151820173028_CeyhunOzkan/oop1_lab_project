#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <string>
#include "List.h"
#include "InputReader.h"
#include "Main.h"

using namespace std;
/*!
* \class OutputWriter
* \brief Includes methods of WriteLine and Delete.
*/
typedef class OutputWriter
{
public:
    static void WriteLine(string fileName, string line);
	static void Delete(string fileName);

}OutputWriter;
