#include <string>
#include <iostream>
#include "List.h"
#include "Utility.h"

using namespace std;


/*!
* \brief Method splits the data by using space character.
* @param data is string variable.
* @return terms as List<string>.
*/
List<string> Utility::Split(string data)
{
	List<string> dts;
	string dt = "";

	for (auto x : data)
	{
		if (x == ' ')
		{
			dts.Add(dt);
			dt = "";
		}
		else
			dt += x;
	}

	if (dt != "")
		dts.Add(dt);

	return dts;
}

/*!
* \brief Method transforms the data in string type into the Codes in enum type
* @param data is string variable.
* @return terms as Codes.
*/
Codes Utility::Transform(string data)
{
	if (data == "start_engine;")
		return Codes::Start_Engine;
	else if (data == "stop_engine;")
		return Codes::Stop_Engine;
	else if (data == "add_fuel_tank")
		return Codes::Add_Fuel_Tank;
	else if (data == "list_fuel_tanks;")
		return Codes::List_Fuel_Tanks;
	else if (data == "connect_fuel_tank_to_engine")
		return Codes::Connect_Fuel_Tank_to_Engine;
	else if (data == "fill_tank")
		return Codes::Fill_Tank;
	else if (data == "remove_fuel_tank")
		return Codes::Remove_Fuel_Tank;
	else if (data == "disconnect_fuel_tank_from_engine")
		return Codes::Disconnect_Fuel_Tank_from_Engine;
	else if (data == "stop_simulation;")
		return Codes::Stop_Simulation;
	else if (data == "open_valve")
		return Codes::Open_Valve;
	else if (data == "print_fuel_tank_count;")
		return Codes::Print_Fuel_Tank_Count;
	else if (data == "print_tank_info")
		return Codes::Print_Tank_Info;
	else if (data == "print_total_fuel_quantity;")
		return Codes::Print_Total_Fuel_Quantity;
	else if (data == "print_total_consumed_fuel_quantity;")
		return Codes::Print_Total_Consumed_Total_Quantity;
	else if (data == "wait")
		return Codes::Wait;
	//...continue
	else
		return Codes::NO_COMMAND;
}

/*!
* \brief Method converts the numbers in the string expression to the double variable.It pays attention to the point.
* @param data is string variable.
* @return string data to double data(just by numbers and point).
*/
double Utility::PureNumber(string data) { // !

	string a = "";
	double def = 0;

	for (auto x : data) {
		if (x >= 46 && x <= 57 && x != 47) {
			a += x;
		}
	}

	try {
		def = stod(a);
	}
	catch (const std::exception&) {

	}
	return def;
}