#pragma once
#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <string>
#include "List.h"
using namespace std;

/*!
* \class InputReader
* \brief Includes InputReader constructor method and GetCommands method.
*/
typedef class InputReader
{
private:
	List<string> commands;

public:
	InputReader(string file);
	List<string> GetCommands() const;

}InputReader;
