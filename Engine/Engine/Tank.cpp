#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <string>
#include "Tank.h"
#include "OutputWriter.h"
using namespace std;

int Tank::ID = 0;

/*!
* \brief default constructor method.
*/
Tank::Tank()
{

}
/*!
* \brief constructor method.
*/
Tank::Tank(double capacity)
{
	this->capacity = capacity;
	this->fuelQuantity = 0;
	this->tankID = to_string(Tank::ID);
	Tank::ID++;
}
/*!
* \brief destructor method.
*/
Tank::~Tank() {

}
/*!
* \brief Method repairs tank.
* @return nothing.
*/
void Tank::RepairTank() {

}
/*!
* \brief Method breaks tank or checks.
* @return nothing.
*/
void Tank::BreakTank() {

}

/*!
* \brief Method checks if the tank is broken.
* @return true or false.
*/
bool Tank::IsBroken() {
	if (this->broken)
		return 1;
	else
		return 0;
}
/*!
* \brief Method sets capacity of tank.
* @param capacity is double variable.
* @return nothing.
*/
void Tank::SetTankCapacity(double capacity) {
	this->capacity = capacity;
}
/*!
* \brief Method gets capacity of tank.
* @return capacity.
*/
double Tank::GetTankCapacity() const {
	return this->capacity;
}
/*!
* \brief Method sets fuel quantity of tank.
* @param fuelQuantity is double variable.
* @return nothing.
*/
void Tank::SetFuelQuantity(double fuelQuantity) {
	this->fuelQuantity = fuelQuantity;
}
/*!
* \brief Method gets fuel quantity of tank.
* @return fuelQuantity.
*/
double Tank::GetFuelQuantity() const {
	return this->fuelQuantity;
}
/*!
* \brief Method gets tank ID.
* @return TankID.
*/
string Tank::GetTankID() const {
	return this->tankID;
}
/*!
* \brief Method does operator overloading of <<.
* @param out is ostream& variable and tank is Tank& variable.
* @return ostream& out.
*/
ostream& operator<<(ostream& out, const Tank& tank) {
	string status, valve;
	if (tank.broken) status = "Solid";
	else status = "Broken";

	if (tank.IsOpenedValve()) valve = "Open";
	else valve = "Close";

	out << "Tank ID: " << tank.tankID << endl
		<< "Capacity: " << tank.capacity << endl
		<< "Fuel Quantity: " << tank.fuelQuantity << endl
		<< "Status: " << status << endl
		<< "Valve: " << valve << endl
		<< "Connected: " << tank.connected << endl;

	OutputWriter::WriteLine(Main::outputFileName, "\n---> Tank ID: " + tank.tankID + "<---\n");
	OutputWriter::WriteLine(Main::outputFileName, "Capacity: " + to_string(tank.capacity) + "\n");
	OutputWriter::WriteLine(Main::outputFileName, "Fuel Quantity: " + to_string(tank.fuelQuantity) + "\n");
	OutputWriter::WriteLine(Main::outputFileName, "Status: " + status + "\n");
	OutputWriter::WriteLine(Main::outputFileName, "Valve: " + valve + "\n");
	OutputWriter::WriteLine(Main::outputFileName, "Connected: " + tank.connected + "\n");

	return out;
}
/*!
* \brief Method sets connected status of tank.
* @param status is string variable.
* @return nothing.
*/
void Tank::SetConnectedStatus(string status) {
	this->connected = status;
}
/*!
* \brief Method gets connected status of tank.
* @return connected status of tank with type of string.
*/
string Tank::GetConnectedStatus() const {
	return this->connected;
}
/*!
* \brief Along with the tank number, it indicates which tank it is and likewise to whom the valve belongs.
* @return connected status of tank with type of string.
*/
void Tank::StopSimulation() {
	string info = "Tank " + this->tankID + ": Simulation stopped\nValve " + this->tankID + ": Simulation stopped\n";
	OutputWriter::WriteLine(Main::outputFileName, info);
	cout << info;
}