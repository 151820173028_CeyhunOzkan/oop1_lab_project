#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <string>
#include "List.h"
#include "InputReader.h"
#include "Main.h"
using namespace std;

/*!
* \brief Constructor method reads inputs.
* @param file is string variable.
* @return nothing.
*/
InputReader::InputReader(string file)
{
	ifstream datafile;
	datafile.open(file, ios::in);

	if (datafile.is_open())
	{
		string line;
		while (!datafile.eof())
		{
			getline(datafile, line);
			if (!line.empty())
				this->commands.Add(line);
		}
	}
	else
		Main::ErrorMessage = "The file is not exist!";
		

	datafile.close();
}

/*!
* \brief Method gets all command list..
* @return list<string> commands.
*/
List<string> InputReader::GetCommands() const
{
	return this->commands;
}