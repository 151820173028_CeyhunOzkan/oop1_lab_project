#pragma once
#include <stdlib.h>
#include <string>
#include <iostream>
#include "List.h"

using namespace std;
/*!
* \enum Codes
* \brief It will continue to be developed on the commands from the user.
*/
typedef enum Codes
{
	Start_Engine = 0,
	Stop_Engine = 1,
	Add_Fuel_Tank = 2,
	List_Fuel_Tanks = 3,
	Print_Fuel_Tank_Count = 4,
	Remove_Fuel_Tank = 5,
	Connect_Fuel_Tank_to_Engine = 6,
	Disconnect_Fuel_Tank_from_Engine = 7,
	List_Connected_Tanks = 8,
	Give_Back_Fuel = 9,
	Print_Total_Fuel_Quantity = 10,
	Print_Total_Consumed_Total_Quantity = 11,
	Print_Tank_Info = 12,
	Fill_Tank = 13,
	Open_Valve = 14,
	Close_Valve = 15,
	Break_Fuel_Tank = 16,
	Repair_Fuel_Tank = 17,
	Wait = 18,
	Stop_Simulation = 19,

	NO_COMMAND = 1000,
};
/*!
* \class Utility
* \brief Includes methods of Split,Transform and PureNumber.
*/
typedef class Utility
{
private:
	Utility();

public:
	static List<string> Split(string data);
	static Codes Transform(string data);
	static double PureNumber(string data);
};