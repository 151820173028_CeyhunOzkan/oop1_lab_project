#pragma once
template <class T>
/*!
* \class List
* \brief Includes constructor methods of List,List(int),List(const List<T> &list).
* \brief Includes methods of Add,GetItem,Insert,Remove,IndexOf,IsEmpty,Length,Clear.
*/
class List
{
private:
	T *M;
	int capacity;
	int length;
public:
	/*!
	* \brief constructor method.
	*/
	List()
	{
		this->M = new T[2];
		this->capacity = 2;
		this->length = 0;
	}
	/*!
	* \brief constructor method.
	* @param capacity is int variable.
	* @return nothing.
	*/
	List(int capacity)
	{
		this->M = new T[capacity];
		this->capacity = capacity;
		this->length = 0;
	}
	/*!
	* \brief Copy constructor for list implementation.Otherwise, the pointer M in T type cannot be transfered another pointer.
	* @param &list is List<T> variable.
	* @return nothing.
	*/
	List(const List<T> &list)
	{
		this->capacity = list.capacity;
		this->length = list.length;

		this->M = new T[list.length];
		for (int i = 0; i < list.length; i++)
			this->M[i] = list.M[i];
	}
	/*!
	* \brief destructor method.
	*/
	~List()
	{
		delete[] this->M;
	}
	/*!
	* \brief Method adds items.
	* @param item is T variable.
	* @return nothing.
	*/
	void Add(T item)
	{
		this->Insert(this->length, item);
	}
	/*!
	* \brief Method gets item.
	* @param index is int variable.
	* @return T.
	*/
	T GetItem(int index) const
	{
		if (index < 0 || index >= this->length)
			throw std::exception("List.Add: Index out of range");

		return this->M[index];
	}
	/*!
	* \brief Method inserts item.
	* @param index is int variable and @param item is T variable.
	* @return nothing.
	*/
	void Insert(int index, T item)
	{
		if (index < 0 || index > this->length)
			throw std::exception("List.Add: Index out of range");

		if (this->length == this->capacity)
		{
			this->capacity = this->capacity * 2;
			T *N = new T[this->capacity];
			for (int i = 0; i < this->length; i++)
				N[i] = this->M[i];
			delete[] this->M;
			this->M = N;
		}

		for (int i = this->length - 1; i >= index; i--)
			this->M[i + 1] = this->M[i];

		this->M[index] = item;
		this->length++;
	}
	/*!
	* \brief Method removes item.
	* @param index is int variable.
	* @return nothing.
	*/
	void Remove(int index)
	{
		if (index < 0 || index >= this->length)
			throw std::exception("List::Remove: Index out of range");

		for (int i = index; i<this->length - 1; i++)
			this->M[i] = this->M[i + 1];

		this->length--;
	}
	/*!
	* \brief Method finds index of item.
	* @param item is T variable.
	* @return IndexOfItem.
	*/
	int IndexOf(T item)
	{
		for (int i = 0; i < this->length; i++)
		{
			if (this->M[i] == item)
				return i;
		}

		return -1;
	}
	/*!
	* \brief Method checks if the list is empty.
	* @return true or false.
	*/
	bool IsEmpty()
	{
		if (this->length == 0)
			return true;
		else
			return false;
	}
	/*!
	* \brief Method gets length of list.
	* @return length.
	*/
	int Length() const
	{
		return this->length;
	}
	/*!
	* \brief Method clears list.
	* @return nothing.
	*/
	void Clear()
	{
		this->length = 0;
	}
};
